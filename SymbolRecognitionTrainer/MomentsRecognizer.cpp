#include "../ANNLib/ANN.h"
#include "MomentsRecognizer.h"


using namespace cv;
using namespace std;
using namespace fe;

MomentsRecognizer::MomentsRecognizer()
{
}


MomentsRecognizer::~MomentsRecognizer()
{
}

std::string MomentsRecognizer::Recognize(fe::ComplexMoments & moments)
{
	Mat output;
	pAnn->predict(MomentsToInput(moments), output);
	return OutputToValue(output);
}

double MomentsRecognizer::PrecisionTest(std::map<std::string, std::vector<fe::ComplexMoments>> moments)
{
	int right_answers = 0;
	int tests = 0;
	for (auto primer = moments.begin(); primer != moments.end(); primer++) {
		for (int j = 0; j < (int)primer->second.size(); j++) {
			auto recognized = Recognize(primer->second[j]);
			if (recognized == primer->first) {
				right_answers++;
			}
			tests++;
		}
	}
	return (double)right_answers / (double)tests;
}

bool MomentsRecognizer::Save(std::string filename)
{
	FileStorage fs(filename, FileStorage::WRITE);
	if (!fs.isOpened()) {
		return false;
	}
	pAnn->write(fs);
	fs << "values" << values;
	fs.release();
	return true;
}

bool MomentsRecognizer::Read(std::string filename)
{
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened()) {
		return false;
	}
	pAnn = ml::ANN_MLP::create();
	pAnn->read(fs.root());
	values.clear();
	for (auto iter = fs["values"].begin(); iter != fs["values"].end(); iter++) {
		values.push_back(*iter);
	}
	fs.release();
	return true;
}

Mat MomentsRecognizer::MomentsToInput(fe::ComplexMoments& moments)
{
	return moments.abs.clone();
}

std::string MomentsRecognizer::OutputToValue(cv::Mat output)
{
	throw std::runtime_error("error");
}

bool MomentsRecognizer::Train(
	std::map<std::string, std::vector<fe::ComplexMoments>> moments,
	std::vector<int> layers,
	int max_iters,
	float eps,
	float speed)
{
	pAnn->create();
	pAnn->setLayerSizes(layers);

	pAnn->setBackpropMomentumScale(0.1);
	pAnn->setBackpropWeightScale(0.1);
	pAnn->setActivationFunction(ml::ANN_MLP::SIGMOID_SYM, 0., 0.);

	TermCriteria term_criteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, max_iters, eps);
	pAnn->setTermCriteria(term_criteria);
	pAnn->setTrainMethod(ml::ANN_MLP::RPROP, 0.001);
	return false;
}