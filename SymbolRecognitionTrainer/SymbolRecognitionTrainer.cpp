#define NMAX 12
#define TSIDE 30

#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "IBlobProcessor.h"

#include "FeatureExtraction.h"
#include "MomentsHelper.h"
#include "MomentsRecognizer.h"

using namespace cv;
using namespace std;
using namespace fe;

std::map< std::string, std::vector<fe::ComplexMoments> > res;

void generateData()
{
	//MomentsHelper::DistributeData("\..\\Data\\labeled_data", "\..\\Data\\ground_data", "\..\\Data\\test_data", 10);
	auto blob = CreateBlobProcessor();
	
	auto polynome = CreatePolynomialManager();
	polynome->InitBasis(NMAX, TSIDE);
	
	MomentsHelper::GenerateMoments("\..\\Data\\ground_data", blob, polynome, res);
	cout << "===Moments generate!===" << endl;
	MomentsHelper::SaveMoments("\..\\Data\\fuck.txt", res);
	cout << "===Generate data!===" << endl;
}

void trainNetwork()
{
	MomentsRecognizer vika = new MomentsHelper();
	std::map< std::string, std::vector<fe::ComplexMoments> > sosi;
	MomentsHelper::ReadMoments("\..\\Data\\fuck.txt", sosi);
	vector<int> conf = { 9, 2, 50, 9 };
	MomentsRecognizer::Train(sosi, conf, 10000, 0.01, 0.1);
	cout << "===Train network!===" << endl;
}

void precisionTest()
{
	cout << "===Precision test!===" << endl;
}

void recognizeImage()
{
	cout << "===Recognize single image!===" << endl;
}

int main(int argc, char** argv)
{
	string key;
	do 
	{
		cout << "===Enter next walues to do something:===" << endl;
		cout << "  '1' - to generate data." << endl;
		cout << "  '2' - to train network." << endl;
		cout << "  '3' - to check recognizing precision." << endl;
		cout << "  '4' - to recognize single image." << endl;
		cout << "  'exit' - to close the application." << endl;
		cin >> key;
		cout << endl;
		if (key == "1") {
			generateData();
		}
		else if (key == "2") {
			trainNetwork();
		}
		else if (key == "3") {
			precisionTest();
		}
		else if (key == "4") {
			recognizeImage();
		}
		cout << endl;
	} while (key != "exit");
	return 0;
}