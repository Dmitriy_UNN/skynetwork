#include <iostream>
#include <ANN.h>
using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "rus");
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<size_t> config;
	config.resize(4);
	config = { 2, 3, 3, 1 };
	auto pAnn = CreateNeuralNetwork(config);
	cout << pAnn->GetType() << endl;
	vector<vector<float>> input, output;
	bool success = LoadData("xor.data", input, output);
	int iter;
	cout << "Iterations: ";
	cin >> iter;
	cout << endl;
	BackPropTraining(pAnn, input, output, iter, 10.e-2, 0.1, true);
	pAnn->Save("skynet.ann");
	system("pause");
	return 0;
}