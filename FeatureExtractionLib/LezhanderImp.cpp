#define FEATURE_DLL_EXPORTS

#include "PolynomialManager.h"
#include "RadialFunctions.h"
#include "FeatureExtraction.h"
#include <iostream>

using namespace fe;
using namespace cv;
using namespace std;
using namespace rf;

//#define DEBUG_OUT

//���������� �������� ��������
class LegendrePolynomialManager : public PolynomialManager
{
	virtual void InitBasis(int n_max, int diameter) override
	{
		//polynomials.resize(n_max + 1);

		Mat matRe = Mat::zeros(diameter, diameter, CV_64FC1);
		Mat matIm = Mat::zeros(diameter, diameter, CV_64FC1);

		double* vectRe = matRe.ptr<double>();
		double* vectIm = matIm.ptr<double>();

		for (int n = 0; n <= n_max; n++)
		{
			polynomials.emplace_back();
			for (int m = 0; m <= n_max; m++)
			{
				for (int i = 0; i < diameter; i++)
				{
					for (int j = 0; j < diameter; j++)
					{
						double x = (j - diameter / 2) / (double)(diameter / 2);
						double y = -(i - diameter / 2) / (double)(diameter / 2);

						double radius = RadialFunctions::ShiftedLegendre(sqrt(y * y + x * x), n);
						double fi = atan2(y, x);

						if (abs(x) < 1e-3 && abs(y) < 1e-3)
							fi = 0;

						vectRe[diameter * i + j] = radius * cos(m * fi);
						vectIm[diameter * i + j] = radius * sin(m * fi);
					}
				}
				polynomials[n].push_back({ matRe.clone(), matIm.clone() });
			}
		}
	}

	virtual ComplexMoments Decompose(Mat blob) override
	{
		Mat blob_doub = Mat::zeros(blob.rows, blob.cols, CV_64FC1);
		blob.convertTo(blob_doub, CV_64FC1, 1.0 / 255 / blob.rows / blob.cols);
#ifdef DEBUG_OUT
		cv::imshow("blob", blob);
		cv::waitKey();
#endif

		int polynomials_count = 0;
		for (auto& polynom_line : polynomials)
		{
			polynomials_count += polynom_line.size();
		}

		ComplexMoments dec;
		dec.abs = Mat::zeros(Size(polynomials_count, 1), CV_64FC1);
		dec.phase = Mat::zeros(Size(polynomials_count, 1), CV_64FC1);
		dec.re = Mat::zeros(Size(polynomials_count, 1), CV_64FC1);
		dec.im = Mat::zeros(Size(polynomials_count, 1), CV_64FC1);

		double *re = dec.re.ptr<double>();
		double *im = dec.im.ptr<double>();
		double *phase = dec.phase.ptr<double>();
		double *abs = dec.abs.ptr<double>();

		size_t idx = 0;
		for (auto& pl : polynomials)
		{
			for (auto& p : pl)
			{
				re[idx] = sum(blob_doub.mul(p.first))[0];
				im[idx] = -sum(blob_doub.mul(p.second))[0];
				//��������
				abs[idx] = sqrt(re[idx] * re[idx] + im[idx] * im[idx]);
				phase[idx] = atan2(im[idx], re[idx]);
				idx++;
			}
		}

		double norm = cv::norm(dec.abs);
		dec.abs /= norm;
		dec.re /= norm;
		dec.im /= norm;

		return dec;
	}

	virtual Mat Recovery(ComplexMoments & decomposition) override
	{
		//��������
		int size = polynomials[0][0].first.cols;

		Mat recov = Mat::zeros(size, size, CV_64FC1);
		int index = 0;

		for (auto& polynom_line : polynomials)
		{
			for (auto& polynom : polynom_line)
			{
				recov += decomposition.re.ptr<double>()[index] * polynom.first
					- decomposition.im.ptr<double>()[index] * polynom.second;
				index++;
			}
		}

		return recov;
	}

	virtual string GetType() override
	{
		return "Lezhander Polynome";
	}
};

shared_ptr<fe::PolynomialManager> fe::CreatePolynomialManager()
{
	return make_shared<LegendrePolynomialManager>();
}