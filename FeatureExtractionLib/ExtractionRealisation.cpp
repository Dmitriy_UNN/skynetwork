#define FEATURE_DLL_EXPORTS

#include "IBlobProcessor.h"
#include "FeatureExtraction.h"
#include "RadialFunctions.h"
#include <iostream>

using namespace std;
using namespace fe;
using namespace cv;

class BlobProcessor :public IBlobProcessor
{
	virtual string GetType() override
	{
		return "Simple Blob Processor by your master";
	}

	virtual vector<Mat> DetectBlobs(Mat image) override
	{
		vector<Mat> res;
		Mat binary(image.rows, image.cols, CV_8UC1);
		threshold(image, binary, 127, 255, THRESH_BINARY_INV);
		//imshow("binary", binary);

		vector<vector<Point>> contour;
		vector<Vec4i> hierarchy;

//#ifdef DEBUG_OUTPUT
		findContours(binary, contour, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

		Mat drawing = Mat::zeros(binary.size(), CV_8UC3);
		RNG rnd(0xFFFFFFFF);
		/*
		for (int idx = 0; idx >= 0; idx = hierarchy[idx][0])
		{
			Scalar color = Scalar(rnd.uniform(0, 255), rnd.uniform(0, 255), rnd.uniform(0, 255));
			drawContours(drawing, contour, idx, color, CV_FILLED, LineTypes::LINE_8, hierarchy);
		}*/

		//imshow("contours", drawing);
//#endif

		for (int idx = 0; idx >= 0; idx = hierarchy[idx][0])
		{
			Point2f center;
			float radius;
			minEnclosingCircle(contour[idx], center, radius);
			res.push_back(Mat::zeros(round((double)radius) * 2, round((double)radius) * 2, CV_8UC1));
			Scalar white = Scalar(255, 255, 255);
			Point2f offset = -center + Point2f(radius, radius);
			drawContours(res.back(), contour, idx, white, CV_FILLED, cv::LineTypes::LINE_8, hierarchy, 255, offset);

			//imshow("res", res.back());
			//waitKey(0);
		}

		return res;
	}

	virtual vector<cv::Mat> NormalizeBlobs(vector<cv::Mat> & blobs,	int side) override
	{
		vector <Mat> normalized;
		for (auto& blob : blobs)
		{
			Mat m;
			resize(blob, m, Size(side, side));
			normalized.push_back(m);
			//imshow("normalized", m);
			//waitKey(0);
		}
		return normalized;
	}
};

shared_ptr<fe::IBlobProcessor> fe::CreateBlobProcessor()
{
	return make_shared<BlobProcessor>();
}