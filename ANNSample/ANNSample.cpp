#include <iostream>
#include <ANN.h>
#include <conio.h>
using namespace std;
using namespace ANN;

int main()
{
	setlocale(LC_ALL, "rus");
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	vector<vector<float>> input, output;
	auto pAnn = CreateNeuralNetwork();
	cout << pAnn->GetType() << endl;
	pAnn->Load("skynet.ann");
	LoadData("xor.data", input, output);
	
	cout << "X\t\t" << "Predicted Y\t" << endl;
	for (int i = 0; i < input.size(); i++)
	{
		cout << input[i][0] << "\t" << input[i][1] << "\t" << pAnn->Predict(input[i])[0] << endl;
	}

	return 0;
}