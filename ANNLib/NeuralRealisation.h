#pragma once
#include "ANN.h"
#include <iostream>

class NeuralRealisation : public ANN::ANeuralNetwork
{
public:
	NeuralRealisation(std::vector<size_t> conf, ANeuralNetwork::ActivationType aType, float scale);

	virtual std::string GetType();
	virtual std::vector<float> Predict(std::vector<float> & input);
	
	~NeuralRealisation();
};

