#include <iostream>
#include <FeatureExtraction.h>
#include "Visualisation.h"

#define NMAX 12
#define TSIDE 30

using namespace std;
using namespace fe;
using namespace cv;

int main() 
{
	cout << "hello world!" << endl;
	cout << fe::GetTestString().c_str() << endl;
	auto blob_proc = CreateBlobProcessor();
	cout << blob_proc->GetType() << endl;
	
	auto image = imread("numbers.png", IMREAD_GRAYSCALE);
	auto blobs = blob_proc->DetectBlobs(image);
	
	auto polynomials = CreatePolynomialManager();
	cout << polynomials->GetType() << endl;
	polynomials->InitBasis(NMAX, TSIDE);
	ShowPolynomials("polynomes", polynomials->GetBasis());
	auto normalized_blobs = blob_proc->NormalizeBlobs(blobs, TSIDE);
	for (auto&blob : normalized_blobs)
	{
		auto dec = polynomials->Decompose(blob);
		auto rec = polynomials->Recovery(dec);
		Show64FC1Mat("recovery", rec);
		waitKey(0);
	}
	
	imshow("src", image);
	waitKey(0);
	
	return 0;
}